/* isAuthenticated.js ~ restrict specified pages & routes to only authenticated/ logged in users. */

module.exports = function(req, res, next) {
    /* Proceed, if the user is properly logged in */
    if (req.user) {
        return next();
    }
    /* If not logged in, re-direct user to Login Page */
    return res.redirect("/");
};