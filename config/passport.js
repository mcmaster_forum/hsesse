/* passport.js ~ configuration for authentication library */
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var db = require("../models"); /* DB connections to verify user details */

//-------->
// Passport.js Local Strategy ~ login with a username/email and password */
passport.use(new LocalStrategy(
    /* User will sign in using email, rather than the default 'username' field */
    {
        usernameField: "email"
    },
    function(email, password, done) {
        /* When a user tries to sign in this code runs */
        db.User.findOne({
            where: {
                email: email
            }
        }).then(function(dbUser) { /* If there's no user with the given email */
            if (!dbUser) {
                return done(null, false, {
                    message: "Incorrect Email."
                });
            }
            /* If correct email BUT password is wrong */
            else if (!dbUser.validPassword(password)) {
                return done(null, false, {
                    message: "Incorrect Password."
                });
            }
            /* Both email AND passwords have a match, so return the user */
            return done(null, dbUser);
        });
    }
));

//----------->
/* To maintain authentication state across HTTP requests, Sequelize.js needs to serialize and deserialize the user */
passport.serializeUser(function(user, cb) {
    cb(null, user);
});
//-->
passport.deserializeUser(function(obj, cb) {
    cb(null, obj);
});

/* ---- Exporting our configured passport --->  */
module.exports = passport;

