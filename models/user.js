/* user.js ~ authentication and user management details */
/* Requiring bcrypt for password hashing. Using the bcrypt-nodejs version as 
//the regular bcrypt module sometimes causes errors on Windows machines */
var bcrypt = require("bcrypt-nodejs");

/* create User model. Export it for use on server */
module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        /* The First Name cannot be blank */
        firstname: {
            type: DataTypes.STRING,
            allowNull: true, /* note2Self: fix this! */
            unique: false,
            validate: {
                isAlpha: true, 
				notEmpty: true, /* note2Self: fix this! */
				len: [2,15]
            }
        },
        /* The Last Name cannot be blank */
        lastname: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: false,
            validate: {
                isAlpha: true, 
				notEmpty: true,  
				len: [2,20]
            }
        },
		
		
        /* The email cannot be null, and must be a proper email before creation */
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        /* The password cannot be null */
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        /* used later for authorization levels */
        userlevel: {
            type: DataTypes.ENUM('regularuser', 'poweruser', 'admin' ),
			defaultValue: 'regularuser', 
            allowNull: false
        },
        /* used later for approver levels */
        approverlevel: {
            type: DataTypes.ENUM('regularuser', 'poweruser', 'admin' ),
			defaultValue: 'regularuser', 
            allowNull: false
        }
    });
    /*
    Creating a custom method for our User model. This will check if an unhashed password entered by the user can be compared to the hashed password stored in our database */
    User.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };
    /* Hooks are automatic methods that run during various phases of the User Model lifecycle. In this case, before a User is created, we will automatically hash their password */
    User.hook("beforeCreate", function(user) {
        user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    });
    return User;
};