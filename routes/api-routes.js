/* api-routes.js ~ database GET & POST database writes  */
// Requiring our models and passport as we've configured it
var db = require("../models");
var passport = require("../config/passport");
//-->
module.exports = function(app) {
    /* Using the passport.authenticate middleware with our local strategy. If the user has valid login credentials, send them to the members page. Otherwise the user will be sent an error */
    app.post("/api/login", passport.authenticate("local"), function(req, res) {
        /* The POST is via JavaScript, so we can't actually redirect that post into a GET request. So we will  send them back to the Home page, which in turn will re-direct them. They won't get this or even be able to access this page if they aren't authorized  */
        res.json("/members");
    });
    // 
    /* --- route ~ signup new user ---> */
    /* New SignUp. The user's password is automatically hashed and stored securely thanks to our prior Sequelize.js configuration. If the user is created successfully, proceed to log the user in, otherwise send back an error */
    app.post("/api/signup", function(req, res) {
        console.log(req.body);
        db.User.create({
            email: req.body.email,
            password: req.body.password
        }).then(function() {
            res.redirect(307, "/api/login");
        }).catch(function(err) {
            console.log(err);
            res.json(err);
            /* res.status(422).json(err.errors[0].message); */
        });
    });
    //
    /* --- route ~ log out ---> */
    app.get("/logout", function(req, res) {
        req.logout();
        res.redirect("/");
    });
    //
    /* --- route ~ get some user data to be used client side ---> */
    app.get("/api/user_data", function(req, res) {
        if (!req.user) {
            /* If the user is not logged in, send back an empty object */
            res.json({});
        } else {
            /* Otherwise send back the user's email and ID.  Sending back a password, even a hashed password, is never a good idea */
            res.json({
                email: req.user.email,
                id: req.user.id
            });
        }
    });
};