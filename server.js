/* server.js ~ start of application  */
/* Health Systems Exchange research articles library.  Built using Node.js, Express framework, PostgreSQL database. Authentication by the Passport.js library. Also uses Bootstrap 4, Google Fonts, and a Fontastic.me generated font. Property of HSE (Health Systems Evidence) - McMaster University.  
 */


// Requiring necessary npm middleware packages 
var express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var session = require("express-session");
// --------->
/* import passport settings, and database settings */
var passport = require("./config/passport");
var db = require("./models");
const PORT = process.env.PORT || 5000; // Setting up port 


// -------->
const siteName = "Health Systems Exchange";
var LocalStrategy = require("passport-local").Strategy;
var logger = require("morgan"); // logging
var flash = require("express-flash"); // for messaging
var nodemailer = require("nodemailer"); /* for sending mail, example in password reset */
var smtpTransport = require("nodemailer-smtp-transport"); /* for smtp */
/* for local user logins & authentication.  Do social media OAuth logins in next phase */
var bcrypt = require("bcrypt-nodejs");
/* encrypt password.  Even if a hacker gains access to our database, he cannot see the real user passwords */
var async = require("async"); /* for waterfall/ cascading events in password reset */
var crypto = require("crypto"); /* for generating random token in password reset. Crypto is now an inbuilt component of Node.js */
require("dotenv").config(); /* loads environment variables from .env file. Safer to store configuration configuration data in .ev file. Ensure to exclude file in .gitnore file */

//----------------------------->
/* Variables for testing only. Use variables in local .env file instead */
var GmailUser = "forum.mcmaster@gmail.com";
var GmailPass = "A10ShunOGr81";



//----------------------------->
/* Create Express App. Then configure Middleware */
var app = express();
app.use(bodyParser.urlencoded({ extended: false })); //For body parser
app.use(bodyParser.json());
app.use(express.static("public")); /* locate images, svgs, CSS, some JavaScript & other static files here */

/* use sessions to keep track of our user's login status */
app.use(session({
    secret: "v23523pathais35alwaysr723i3472d88c9n0247f99dn9dn29dn70xb90302bd23unferxigt",
    /* secret: "keyboard cat", */
    resave: true,
    saveUninitialized: true
}));
//----------------------------->
/* User authentication via Passport.js library. Local authentication only. (Add Scoial Authentication later) */
app.use(passport.initialize());
app.use(passport.session());



//----- Routing ---------->
/* See also 'routesFile' sub-folder. File 'api-routes.js' writes to the DB. */
/* requiring our routes */
require("./routes/html-routes.js")(app); /* Or use a SPA framework for front-end */
require("./routes/api-routes.js")(app);


// for testing only.  see if server working OK.
/*
app.get('/', function(req, res) {
    res.send("Welcome to our Passport.js and Sequelize ORM based web app!");
});
*/


//----------------------------->
/* Sync DB, and start our magnficent web app   */
db.sequelize.sync().then(function() {
    app.listen(PORT, function(err) {
        if (!err)
            console.log(`${siteName} web application now started, and available on port ${PORT} at http://localhost:${PORT}/ Enjoy!`);
        else console.log(err)
    });
});

//----------------------------->